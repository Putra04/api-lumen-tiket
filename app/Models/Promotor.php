<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promotor extends Model
{
    use SoftDeletes;

    protected $table = 'promotor';

    protected $fillable = [
        'name',
        'phone_number',
        'attachment',
        'address',
        'attatchment_legalitas',
        'flag',
        'created_by',
        'created_at',
        'update_at'
    ];

    protected $dates = ['deleted_at'];

    public function event()
    {
        return $this->hasMany(Event::class, 'id_promotor_created');
    }

    public function user()
    {
        return $this->hasMany(User::class, 'id_promotor');
    }
}
