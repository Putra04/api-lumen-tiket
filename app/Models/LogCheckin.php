<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogCheckin extends Model
{
    use SoftDeletes;

    protected $table = 'log_checkin';

    protected $fillable = [
        'id_barcode',
        'status',
        'flag',
        'created_by',
        'created_at',
        'update_at'
    ];

    protected $dates = ['deleted_at'];
}
