<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Talent extends Model
{
    use SoftDeletes;

    protected $table = 'talent';

    protected $fillable = [
        'name',
        'photo',
        'id_promotor_created',
        'flag',
        'created_by',
        'created_at',
        'update_at'
    ];

    protected $dates = ['deleted_at'];

    public function event()
    {
        return $this->hasMany(Event::class, 'id_talent');
    }
}
