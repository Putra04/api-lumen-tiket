<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use SoftDeletes;

    protected $table = 'order_detail';

    protected $fillable = [
        'id_ticket',
        'qty',
        'subtotal',
        'id_customer',
        'flag',
        'created_by',
        'created_at',
        'update_at'
    ];

    protected $dates = ['deleted_at'];

    public function order()
    {
        return $this->hasMany(Order::class, 'id_order_detail');
    }
}
