<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;

    protected $table = 'customer';

    protected $fillable = [
        'name',
        'email',
        'phone_number',
        'gender',
        'birthday',
        'flag',
        'created_by',
        'created_at',
        'update_at'
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->hasMany(User::class, 'id_customer');
    }
}
