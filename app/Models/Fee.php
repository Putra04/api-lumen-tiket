<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fee extends Model
{
    use SoftDeletes;

    protected $table = 'fee';

    protected $fillable = [
        'name',
        'fee',
        'flag',
        'created_by',
        'created_at',
        'update_at'
    ];

    protected $dates = ['deleted_at'];

    public function order()
    {
        return $this->hasMany(Order::class, 'id_fee');
    }
}
