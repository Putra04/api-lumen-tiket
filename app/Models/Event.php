<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;

    protected $table = 'event';

    protected $fillable = [
        'id_talent',
        'title',
        'desc',
        'date',
        'location',
        'sk',
        'tag',
        'id_promotor_created',
        'img_layout',
        'slug',
        'flag',
        'created_by',
        'created_at',
        'update_at'
    ];

    protected $dates = ['deleted_at'];

    public function talent()
    {
        return $this->belongsTo(Talent::class, 'id_talent');
    }

    public function promotor()
    {
        return $this->belongsTo(Promotor::class, 'id_promotor_created');
    }
}
