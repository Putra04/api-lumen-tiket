<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Barcode extends Model
{
    use SoftDeletes;

    protected $table = 'barcode';

    protected $fillable = [
        'id_order',
        'id_event',
        'number',
        'image',
        'id_promotor_created',
        'flag',
        'created_by',
        'created_at',
        'update_at'
    ];

    protected $dates = ['deleted_at'];
}
