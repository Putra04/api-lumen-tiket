<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
    use SoftDeletes;

    protected $table = 'banner';

    protected $fillable = [
        'title',
        'slug',
        'desc',
        'img',
        'flag',
        'created_by',
        'created_at',
        'update_at'
    ];

    protected $dates = ['deleted_at'];
}
