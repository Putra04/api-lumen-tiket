<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{
    use SoftDeletes;

    protected $table = 'type';

    protected $fillable = [
        'name',
        'flag',
        'created_by',
        'created_at',
        'update_at'
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->hasMany(User::class, 'id_type');
    }
}
