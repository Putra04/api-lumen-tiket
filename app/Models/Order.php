<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $table = 'order';

    protected $fillable = [
        'id_order_detail',
        'disc',
        'id_fee',
        'tax',
        'grandtotal',
        'status',
        'flag',
        'created_by',
        'created_at',
        'update_at'
    ];

    protected $dates = ['deleted_at'];

    public function order_detail()
    {
      return $this->belongsTo(OrderDetail::class, 'id_order_detail');
    }

    public function fee()
    {
      return $this->belongsTo(Fee::class, 'id_fee');
    }
}
