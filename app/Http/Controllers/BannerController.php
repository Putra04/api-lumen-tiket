<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Banner;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Google\Cloud\Storage\StorageClient;
use Illuminate\Support\Facades\Cache;

class BannerController extends BaseController
{
  public function __construct()
  {
    // $this->middleware('auth:api', ['except' => ['store', 'index', 'detail', 'update', 'destroy']]);
  }

  /**
   * Get a JWT via given credentials.
   *
   * @param  Request  $request
   * @return Response
   */

  public function index(Request $request)
  {
    try {
        // $cacheKey = 'banner_' . md5(serialize($request->all()));

        // Cek apakah data ada di cache Redis
        $data = Cache::remember('banner', 5 * 60, function () use ($request) {
            $data = Banner::query();
            $field = DB::getSchemaBuilder()->getColumnListing('banner');

            if ($request->search) {
                $search = $request->search ?? '';
                $data = $data->whereHas('banner', function ($query) use ($search) {
                    $fieldProduct = DB::getSchemaBuilder()->getColumnListing('banner');
                    $query->where(DB::raw('LOWER(CAST(' . $fieldProduct[0] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
                    for ($i = 1; $i < count($fieldProduct); $i++) {
                        $query->orWhere(DB::raw('LOWER(CAST(' . $fieldProduct[$i] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
                    }
                })->orWhere(function ($query) use ($search, $field) {
                    for ($i = 0; $i < count($field); $i++) {
                        $query->orWhere(DB::raw('LOWER(CAST(' . $field[$i] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
                    }
                });
            }

            if ($request->has('filter') && is_array($request->input('filter'))) {
                $filter = $request->input('filter');
                $data->where(function ($query) use ($filter) {
                    foreach ($filter as $key => $value) {
                        $query->where(DB::raw('LOWER(CAST(' . $key . ' AS TEXT))'), strtolower($value));
                    }
                });
            }

            $data = $data->orderBy($request->orderBy ?? 'id', $request->orderSort ?? 'asc');
            $data = $data->paginate($request->limit ?? $data->count(), ['*'], 'page', $request->page ?? '1');

            if (is_null($data)) {
                return null;
            }

            $data->getCollection()->transform(function ($banner) {
                $bannerArray = $banner->toArray();
                $users = DB::table('users')->whereId($banner->created_by)->first();

                $storageClient = new StorageClient([
                    'projectId' => env('GOOGLE_CLOUD_PROJECT_ID'),
                    'keyFilePath' => base_path(env('GOOGLE_CLOUD_KEY_FILE', 'edc-hepytech-9561c5f11ba9.json')),
                ]);

                $bucketName = env('GOOGLE_CLOUD_STORAGE_BUCKET');
                $objectName = $banner->img;

                $img_url = "https://storage.googleapis.com/{$bucketName}/{$objectName}";

                return array_merge($bannerArray, [
                    'img' => $img_url,
                    'created_by' => [
                        'id' => $users->id ?? $banner->created_by ?? null,
                        'name' => $users->name ?? null,
                    ],
                ]);
            });

            return $data;
        });

        if (is_null($data)) {
            return $this->sendError('Banner not found.');
        }

        return $this->sendResponse($data, 'All records retrieved successfully.');
    } catch (\Throwable $th) {
        return $this->sendError($th->getMessage());
    }
  }

  public function store(Request $request)
  {
    try {
      $input = $request->all();

      $validator = Validator::make($input, [
        "title" => 'required',
        "desc"      => 'required',
        "img"      => 'required|mimes:svg,png,jpg,jpeg',
      ]);

      if ($validator->fails()) {
        return $this->sendError($validator->errors()->first());
      }

      $filename = '';
        if ($request->hasFile('img')) {
          if ($request->id != "0") {
            $find = Banner::find($request->id);
            if ($find && Storage::disk('gcs')->exists($find->img)) {
              Storage::disk('gcs')->delete($find->img);
            }
          }
          
          $filename = $this->uploadFile(
            $request->file('img'),
            Str::random(25) . '-event-layout-' . date('dmy')
          );
        } else {
          $filename = $request->img_old ?? '';
        }

      DB::beginTransaction();
      $form_auth = array(
        'title'        => $request->title,
        'slug'        => Str::slug($request->title,'-'),
        'desc'     => $request->desc,
        'img'        => $filename,
        'flag'        => '1',
        'created_at'  => date('Y-m-d H:i:s')
      );
      Banner::insertGetId($form_auth);
      DB::Commit();
      return $this->sendResponse($form_auth, 'Banner created successfully.');
    } catch (\Throwable $th) {
      DB::rollback();
      return $this->sendError($th->getMessage());
    }
  }

  public function detail($id)
  {
    try {
      $data = Banner::where('id', $id)->first();
      if (is_null($data)) {
        return $this->sendError('Banner not Found.', 404);
      }

      $dataArray  = $data->toArray();
      $users      = DB::table('users')->whereId($data->created_by)->first();
      $transformedData = array_merge($dataArray, [
        'img'        => isset($data->img) ? URL::to('/') . 'public/uploads/banner' . $data->img : null,
        'created_by'  => [
          'id'          => $users->id ?? $data->created_by ?? null,
          'name'        => $users->name ?? null,
        ],
      ]);

      return $this->sendResponse($transformedData, 'Detail Banner successfully');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  public function update(Request $request, $id = null)
  {
    try {
      $input = $request->all();

      $validator = Validator::make($input, [
        "title" => 'required',
        "desc"      => 'required',
        "img"      => 'required|mimes:svg,png,jpg,jpeg',
      ]);

      if ($validator->fails()) {
        return $this->sendError($validator->errors()->first());
      }

      $img = null;

      if ($request->hasFile('img')) {
        $file = $request->file('img');
        if ($file->isValid()) {
          $maxSize = 5 * 1024 * 1024; // Batas ukuran file 5MB
          if ($file->getSize() > $maxSize) {
            return $this->sendError('File size exceeds the maximum allowed size.');
          }
          $img  = time() . '.' . $file->getClientOriginalExtension();
          $file->move(base_path('public/uploads/banner'), $img);
        } else {
          return $this->sendError('Invalid Image file.');
        }
      }

      $data = banner::whereId($id)->first();
      $form = array(
        'title'     => $request->title ?? $data->title,
        'slug'          => Str::slug($request->title, '-'),
        'desc'          => $request->desc ?? $data->desc,
        'updated_at'    => date('Y-m-d H:i:s')
      );

      if ($img !== null) {
        $form['img'] = $img;
      }

      DB::table('banner')->where('id', $id)->update($form);

      return $this->sendResponse($form, 'Banner updated successfully.');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  public function destroy(Request $request, $id = null)
  {
    try {
      $banner = Banner::find($id);

      if (!$banner) {
        throw new \Exception("Banner with ID $id not found / has been deleted");
      }

      $form = array(
        'title' => $banner->title,
        'slug' => $banner->slug,
        'desc' => $banner->desc,
      );

      $banner->delete();

      return $this->sendResponse($form, 'Banner delete successfully.');
    } catch (\Throwable $th) {
      return $this->sendError($th->getMessage());
    }
  }
}
