<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Google\Cloud\Storage\StorageClient;
use Illuminate\Support\Facades\Cache;

class OrderController extends BaseController
{


  public function __construct()
  {
    // $this->middleware('auth:api', ['except' => ['store', 'index', 'detail', 'update', 'destroy']]);
  }
  /**
   * Get a JWT via given credentials.
   *
   * @param  Request  $request
   * @return Response
   */

  public function index(Request $request)
  {
    try {
      // $cacheKey = 'order_' . md5(serialize($request->all()));

      // Cek apakah data ada di cache Redis
      $data = Cache::remember('order', 5 * 60, function () use ($request) {

        $data = Order::query();
        $field = DB::getSchemaBuilder()->getColumnListing('order');

        if ($request->search) :
          $search = $request->search ?? '';
          $data = $data->whereHas('order_detail', function ($query) use ($search) {
            $fieldProduct = DB::getSchemaBuilder()->getColumnListing('order_detail');
            $query->where(DB::raw('LOWER(CAST(' . $fieldProduct[0] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
            for ($i = 1; $i < count($fieldProduct); $i++) :
              $query->orWhere(DB::raw('LOWER(CAST(' . $fieldProduct[$i] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
            endfor;
          })->orWhereHas('fee', function ($query) use ($search) {
            $fieldProduct = DB::getSchemaBuilder()->getColumnListing('fee');
            $query->where(DB::raw('LOWER(CAST(' . $fieldProduct[0] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
            for ($i = 1; $i < count($fieldProduct); $i++) :
              $query->orWhere(DB::raw('LOWER(CAST(' . $fieldProduct[$i] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
            endfor;
          })->orWhere(function ($query) use ($search, $field) {
            for ($i = 0; $i < count($field); $i++) :
              $query->orWhere(DB::raw('LOWER(CAST(' . $field[$i] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
            endfor;
          });
        endif;

        if ($request->has('filter') && is_array($request->input('filter'))) {
          $filter = $request->input('filter');
          $data->where(function ($query) use ($filter) {
            foreach ($filter as $key => $value) {
              $query->where(DB::raw('LOWER(CAST(' . $key . ' AS TEXT))'), strtolower($value));
            }
          });
        }

        $data = $data->orderBy($request->orderBy ?? 'id', $request->orderSort ?? 'asc');
        $data = $data->paginate($request->limit ?? $data->count(), ['*'], 'page', $request->page ?? '1');


        if (is_null($data)) {
          return null;
        }


        $data->getCollection()->transform(function ($order) {
          $orderArray = $order->toArray();
          $users = DB::table('users')->whereId($order->created_by)->first();
          $orderdetails = $order->order_detail()->first();
          $fees = $order->fee()->first();
          return array_merge($orderArray, [
            'id_order_detail' => array(
              'id'            => $orderdetails->id ?? $order->id_order_detail ?? null,
              'subtotal'  => $orderdetails->subtotal ?? null,
            ),
            'id_fee' => array(
              'id'            => $fees->id ?? $order->id_fee ?? null,
              'created_at'  => $fees->created_at ?? null,
            ),
            'created_by' => array(
              'id'            => $users->id ?? $order->created_by ?? null,
              'name'          => $users->name ?? null,
            ),
          ]);
        });

        return $data;

      });

      if (is_null($data)) {
        return $this->sendError('Order not found.');
      }

      return $this->sendResponse($data->toArray(), 'All records retrieved successfully.');
    } catch (\Throwable $th) {
      return $this->sendError($th->getMessage());
    }
  }

  public function store(Request $request)
  {
    try {
      $input = $request->all();

      $validator = Validator::make($input, [
        "id_order_detail" => 'required',
        "disc"       => 'required',
        "id_fee"       => 'required',
        "tax"       => 'required',
        "grandtotal"       => 'required',
        "status"       => 'required|in:Pending,Onprocess,Paid,Cancel',
      ]);

      if ($validator->fails()) {
        return $this->sendError($validator->errors()->first());
      }

      $id_order_detail = DB::table('order_detail')->where('deleted_at', null)->where('id', $request->id_order_detail)->first();

      if (!$id_order_detail) {
        return $this->sendError('Invalid Order Detail.');
      }
      
      $id_fee = DB::table('fee')->where('deleted_at', null)->where('id', $request->id_fee)->first();

      if (!$id_fee) {
        return $this->sendError('Invalid Fee.');
      }

      // DB::beginTransaction();
      $form_auth = array(
        'id_order_detail' => $id_order_detail->id,
        'disc' => $request->disc,
        'id_fee' => $id_fee->id,
        'tax' => $request->tax,
        'grandtotal' => $request->grandtotal,
        'status' => $request->status,
        'flag'       => '1',
        'created_at'        => date('Y-m-d H:i:s')
      );

      Order::insertGetId($form_auth);
      // DB::Commit();
      return $this->sendResponse($form_auth, 'Order created successfully.');
    } catch (\Throwable $th) {
      // DB::rollback();
      return $this->sendError($th->getMessage());
    }
  }

  public function detail($id)
  {
    try {
      $result = [];

      $data   = Order::where('id', $id)->first();

      if (is_null($data)) {
        return $this->sendError('Order not found.');
      }

      $dataArray = $data->toArray();
      $users = DB::table('users')->whereId($data->created_by)->first();
      $orderdetails = $data->order_detail()->first();
      $fees = $data->fee()->first();
      $transformedData = array_merge($dataArray, [
        'id_order_dedtail' => array(
          'id'            => $data->id ?? $data->id_order_dedtail ?? null,
          'subtotal'          => $data->subtotal ?? null,
        ),
        'id_fee' => array(
          'id'            => $data->id ?? $data->id_fee ?? null,
          'name'          => $data->name ?? null,
        ),
        'created_by' => [
          'id'   => $users->id ?? $data->created_by ?? null,
          'name' => $users->name ?? null,
        ],
      ]);

      return $this->sendResponse($transformedData, 'Order by Detail retrieved successfully.');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  public function update(Request $request, $id = null)
  {
    try {
      $input = $request->all();

      $validator = Validator::make($input, [
        "id_order_detail" => 'required',
        "disc"       => 'required',
        "id_fee"       => 'required',
        "tax"       => 'required',
        "grandtotal"       => 'required',
        "status"       => 'required|in:Pending, Onprocess, Paid, Cancel',
      ]);

      if ($validator->fails()) {
        return $this->sendError($validator->errors()->first());
      }

      $orderdetailid = DB::table('order_detail')->where('deleted_at', null)->find($request->id_order_detail);
      if (!$orderdetailid) {
        return $this->sendError('Invalid Order Detail.');
      }

      $feeid = DB::table('fee')->where('deleted_at', null)->find($request->id_fee);
      if (!$feeid) {
        return $this->sendError('Invalid Talent.');
      }

      $data = Order::whereId($id)->first();
      $form = array(
        'id_order_detail' => $request->id_order_detail ?? $data->id_order_detail,
        'disc' => $request->disc ?? $data->disc,
        'id_fee' => $request->id_fee ?? $data->id_fee,
        'tax' => $request->tax ?? $data->tax,
        'grandtotal' => $request->grandtotal ?? $data->grandtotal,
        'status' => $request->status ?? $data->status,
        'updated_at'        => date('Y-m-d H:i:s')
      );

      DB::table('order')->where('id', $id)->update($form);

      return $this->sendResponse($form, 'Order updated successfully.');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  public function destroy(Request $request, $id = null)
  {
    try {
      $order = Order::find($id);

      if (!$order) {
        throw new \Exception("Order with ID $id not found / has been deleted");
      }

      $form = array(
        'id_order_detail' => $order->id_order_detail,
        'disc' => $order->disc,
        'id_fee' => $order->id_fee,
        'tax' => $order->tax,
        'grandtotal' => $order->grandtotal,
        'status' => $order->status,
      );

      $order->delete();

      return $this->sendResponse($form, 'Order delete successfully.');
    } catch (\Throwable $th) {
      return $this->sendError($th->getMessage());
    }
  }
}
