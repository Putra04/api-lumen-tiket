<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Promotor;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Google\Cloud\Storage\StorageClient;
use Illuminate\Support\Facades\Cache;

class PromotorController extends BaseController
{
  public function __construct()
  {
    // $this->middleware('auth:api', ['except' => ['store', 'index', 'detail', 'update', 'destroy']]);
  }

  /**
   * Get a JWT via given credentials.
   *
   * @param  Request  $request
   * @return Response
   */

  public function index(Request $request)
  {
    try {
      // $cacheKey = 'promotor_' . md5(serialize($request->all()));

      // Cek apakah data ada di cache Redis
      $data = Cache::remember('promotor', 5 * 60, function () use ($request) {

        $data = Promotor::query();
        $field = DB::getSchemaBuilder()->getColumnListing('promotor');

        if ($request->search) :
          $search = $request->search ?? '';
          $data = $data->whereHas('promotor', function ($query) use ($search) {
            $fieldProduct = DB::getSchemaBuilder()->getColumnListing('promotor');
            $query->where(DB::raw('LOWER(CAST(' . $fieldProduct[0] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
            for ($i = 1; $i < count($fieldProduct); $i++) :
              $query->orWhere(DB::raw('LOWER(CAST(' . $fieldProduct[$i] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
            endfor;
          })->orWhere(function ($query) use ($search, $field) {
            for ($i = 0; $i < count($field); $i++) :
              $query->orWhere(DB::raw('LOWER(CAST(' . $field[$i] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
            endfor;
          });
        endif;

        if ($request->has('filter') && is_array($request->input('filter'))) {
          $filter = $request->input('filter');
          $data->where(function ($query) use ($filter) {
            foreach ($filter as $key => $value) {
              $query->where(DB::raw('LOWER(CAST(' . $key . ' AS TEXT))'), strtolower($value));
            }
          });
        }

        $data = $data->orderBy($request->orderBy ?? 'id', $request->orderSort ?? 'asc');
        $data = $data->paginate($request->limit ?? $data->count(), ['*'], 'page', $request->page ?? '1');

        if (is_null($data)) {
          return null;
        }

        $data->getCollection()->transform(function ($promotor) {
          $promotorArray  = $promotor->toArray();
          $users      = DB::table('users')->whereId($promotor->created_by)->first();

          $storageClient = new StorageClient([
            'projectId' => env('GOOGLE_CLOUD_PROJECT_ID'),
            'keyFilePath' => base_path(env('GOOGLE_CLOUD_KEY_FILE', 'edc-hepytech-9561c5f11ba9.json')),
          ]);

          // Dapatkan nama bucket dan objek
          $bucketName = env('GOOGLE_CLOUD_STORAGE_BUCKET');
          $objectNames = $promotor->attachment; // Sesuaikan dengan struktur penyimpanan Anda di GCS
          $objectName = $promotor->attachment_legalitas; // Sesuaikan dengan struktur penyimpanan Anda di GCS
          
          // Bangun URL dasar
          $attachment_url = "https://storage.googleapis.com/{$bucketName}/{$objectNames}";
          $attachment_legalitas_url = "https://storage.googleapis.com/{$bucketName}/{$objectName}";

          return array_merge($promotorArray, [
            'attachment'        => $attachment_url,
            'attachment_legalitas'        => $attachment_legalitas_url,
            'created_by'  => array(
              'id'    => $users->id ?? $promotor->created_by ?? null,
              'name'  => $users->name ?? null,
            ),
          ]);
        });

        return $data;

      });

      if (is_null($data)) {
        return $this->sendError('Promotor not found.');
      }

      return $this->sendResponse($data, 'All records retrieved successfully.');
    } catch (\Throwable $th) {
      return $this->sendError($th->getMessage());
    }
  }

  public function store(Request $request)
  {
    try {
      $input = $request->all();

      $validator = Validator::make($input, [
        "name" => 'required',
        "phone_number"      => 'required',
        "attachment"      => 'required|mimes:svg,png,jpg,jpeg',
        "address"      => 'required',
        "attachment_legalitas"      => 'required|mimes:svg,png,jpg,jpeg',
      ]);

      if ($validator->fails()) {
        return $this->sendError($validator->errors()->first());
      }

      $fileattachment = '';
        if ($request->hasFile('attachment')) {
          if ($request->id != "0") {
            $find = Promotor::find($request->id);
            if ($find && Storage::disk('gcs')->exists($find->attachment)) {
              Storage::disk('gcs')->delete($find->attachment);
            }
          }
          
          $fileattachment = $this->uploadFile(
            $request->file('attachment'),
            Str::random(25) . '-event-layout-' . date('dmy')
          );
        } else {
          $fileattachment = $request->attachment_old ?? '';
        }
        
      $legal = '';
        if ($request->hasFile('attachment_legalitas')) {
          if ($request->id != "0") {
            $find = Promotor::find($request->id);
            if ($find && Storage::disk('gcs')->exists($find->attachment_legalitas)) {
              Storage::disk('gcs')->delete($find->attachment_legalitas);
            }
          }
          
          $legal = $this->uploadFile(
            $request->file('attachment_legalitas'),
            Str::random(25) . '-event-layout-' . date('dmy')
          );
        } else {
          $legal = $request->attachment_legalitas_old ?? '';
        }

      DB::beginTransaction();
      $form_auth = array(
        'name'        => $request->name,
        'phone_number'     => $request->phone_number,
        'attachment'        => $fileattachment,
        'address'     => $request->address,
        'attachment_legalitas'        => $legal,
        'flag'        => '1',
        'created_at'  => date('Y-m-d H:i:s')
      );
      Promotor::insertGetId($form_auth);
      DB::Commit();
      return $this->sendResponse($form_auth, 'Promotor created successfully.');
    } catch (\Throwable $th) {
      DB::rollback();
      return $this->sendError($th->getMessage());
    }
  }

  public function detail($id)
  {
    try {
      $data = Promotor::where('id', $id)->first();
      if (is_null($data)) {
        return $this->sendError('Promotor not Found.', 404);
      }

      $dataArray  = $data->toArray();
      $users      = DB::table('users')->whereId($data->created_by)->first();
      $transformedData = array_merge($dataArray, [
        'attachment'        => isset($data->attachment) ? URL::to('/') . 'public/uploads/promotor' . $data->attachment : null,
        'attachment_legalitas'        => isset($data->attachment_legalitas) ? URL::to('/') . 'public/uploads/promotor' . $data->attachment_legalitas : null,
        'created_by'  => [
          'id'          => $users->id ?? $data->created_by ?? null,
          'name'        => $users->name ?? null,
        ],
      ]);

      return $this->sendResponse($transformedData, 'Detail Promotor successfully');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  public function update(Request $request, $id = null)
  {
    try {
      $input = $request->all();

      $validator = Validator::make($input, [
        "name" => 'required',
        "phone_number"      => 'required',
        "attachment"      => 'required|mimes:svg,png,jpg,jpeg',
        "address"      => 'required',
        "attachment_legalitas"      => 'required|mimes:svg,png,jpg,jpeg',
      ]);

      if ($validator->fails()) {
        return $this->sendError($validator->errors()->first());
      }

      $attachment = null;
      $attachment_legal = null;

      if ($request->hasFile('attachment')) {
        $file = $request->file('attachment');
        if ($file->isValid()) {
          $maxSize = 5 * 1024 * 1024; // Batas ukuran file 5MB
          if ($file->getSize() > $maxSize) {
            return $this->sendError('File size exceeds the maximum allowed size.');
          }
          $attachment  = time() . '.' . $file->getClientOriginalExtension();
          $file->move(base_path('public/uploads/banner'), $attachment);
        } else {
          return $this->sendError('Invalid Image file.');
        }
      }

      if ($request->hasFile('attachment_legalitas')) {
        $file = $request->file('attachment_legalitas');
        if ($file->isValid()) {
          $maxSize = 5 * 1024 * 1024; // Batas ukuran file 5MB
          if ($file->getSize() > $maxSize) {
            return $this->sendError('File size exceeds the maximum allowed size.');
          }
          $attachment_legal  = time() . '.' . $file->getClientOriginalExtension();
          $file->move(base_path('public/uploads/promotor'), $attachment_legal);
        } else {
          return $this->sendError('Invalid Image file.');
        }
      }

      $data = Promotor::whereId($id)->first();
      $form = array(
        'name'     => $request->name ?? $data->name,
        'phone_number'          => $request->phone_number ?? $data->phone_number,
        'address'          => $request->address ?? $data->address,
        'updated_at'    => date('Y-m-d H:i:s')
      );

      if ($attachment !== null) {
        $form['attachment'] = $attachment;
      }

      if ($attachment_legal !== null) {
        $form['attachment_legalitas'] = $attachment_legal;
      }

      DB::table('promotor')->where('id', $id)->update($form);

      return $this->sendResponse($form, 'Promotor updated successfully.');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  public function destroy(Request $request, $id = null)
  {
    try {
      $promotor = Promotor::find($id);

      if (!$promotor) {
        throw new \Exception("Promotor with ID $id not found / has been deleted");
      }

      $form = array(
        'name' => $promotor->name,
        'phone_number' => $promotor->phone_number,
        'address' => $promotor->address,
      );

      $promotor->delete();

      return $this->sendResponse($form, 'Promotor delete successfully.');
    } catch (\Throwable $th) {
      return $this->sendError($th->getMessage());
    }
  }
}
