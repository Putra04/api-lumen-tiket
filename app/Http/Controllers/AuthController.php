<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use  App\Models\User;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;

class AuthController extends BaseController
{


    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'refresh', 'logout', 'store', 'index']]);
    }
    /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function login(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['email', 'password']);

        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

     /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => auth()->user(),
            'expires_in' => auth()->factory()->getTTL() * 60 * 24
        ]);
    }

    public function store(Request $request)
    {
      try {
        $input = $request->all();

        $validator = Validator::make($input, [
          "name"      => 'required',
          "email"         => 'required',
          "id_type"      => 'required',
          "id_promotor"          => 'required',
          "password"       => 'required',
          "id_customer"       => 'required',

        ]);

        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
          return $this->sendError('Not a valid format email, Check Again.');
        }

        $name = User::where('name', $request->name)->first();
        if ($name) {
          return $this->sendError('Name already used, Check Again.');
        }

        $email = User::where('email', $request->email)->first();
        if ($email) {
          return $this->sendError('email already used, Check Again.');
        }

        if ($validator->fails()) {
          return $this->sendError($validator->errors()->first());
        }

        // DB::beginTransaction();
        $form_auth = array(
          'name'          => $request->name ?? "",
          'email'             => $request->email ?? "",
          'id_type'              => $request->id_type ?? "",
          'id_promotor'           => $request->id_promotor ?? "",
          'password'          => Hash::make($request->password) ?? "",
          'id_customer'        => $request->id_customer ?? "",
          'flag'              => '1',
          'created_at'        => date('Y-m-d H:i:s')
        );
        DB::table('users')->insertGetId($form_auth);
        // DB::Commit();
        return $this->sendResponse($form_auth, 'Register created successfully.');
      } catch (\Throwable $th) {
        // DB::rollback();
        return $this->sendError($th->getMessage());
      }
    }

    public function index(Request $request)
    {
      try {
        // $cacheKey = 'users_' . md5(serialize($request->all()));

        // Cek apakah data ada di cache Redis
        $data = Cache::remember('users', 5 * 60, function () use ($request) {

          $data = User::query();
          $field = DB::getSchemaBuilder()->getColumnListing('users');

          if ($request->search) :
            $search = $request->search ?? '';
            $data = $data->whereHas('type', function ($query) use ($search) {
              $fieldProduct = DB::getSchemaBuilder()->getColumnListing('type');
              $query->where(DB::raw('LOWER(CAST(' . $fieldProduct[0] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
              for ($i = 1; $i < count($fieldProduct); $i++) :
                $query->orWhere(DB::raw('LOWER(CAST(' . $fieldProduct[$i] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
              endfor;
            })->orWhereHas('promotor', function ($query) use ($search) {
              $fieldProduct = DB::getSchemaBuilder()->getColumnListing('promotor');
              $query->where(DB::raw('LOWER(CAST(' . $fieldProduct[0] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
              for ($i = 1; $i < count($fieldProduct); $i++) :
                $query->orWhere(DB::raw('LOWER(CAST(' . $fieldProduct[$i] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
              endfor;
            })->orWhereHas('customer', function ($query) use ($search) {
              $fieldProduct = DB::getSchemaBuilder()->getColumnListing('customer');
              $query->where(DB::raw('LOWER(CAST(' . $fieldProduct[0] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
              for ($i = 1; $i < count($fieldProduct); $i++) :
                $query->orWhere(DB::raw('LOWER(CAST(' . $fieldProduct[$i] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
              endfor;
            })->orWhere(function ($query) use ($search, $field) {
              for ($i = 0; $i < count($field); $i++) :
                $query->orWhere(DB::raw('LOWER(CAST(' . $field[$i] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
              endfor;
            });
          endif;

          if (isset($request->filter) && count($request->filter) > 0) {
            $filter = $request->filter;
            $data->where(function ($data) use ($filter) {
              foreach ($filter as $key => $value) {
                $data->where(DB::raw('LOWER(CAST(' . $key . ' AS TEXT))'), strtolower($value));
              }
            });
          }

          $data = $data->orderBy($request->orderBy ?? 'id', $request->orderSort ?? 'asc');
          $data = $data->paginate($request->limit ?? $data->count(), ['*'], 'page', $request->page ?? '1');


          if (is_null($data)) {
            return null;
          }

          $data->getCollection()->transform(function ($user) {
            $userArray = $user->toArray();
            $users = DB::table('users')->whereId($user->created_by)->first();
            $types = $user->type()->first();
            $promotors = $user->promotor()->first();
            $customers = $user->customer()->first();
            return array_merge($userArray, [
              'id_type' => array(
                'id'            => $types->id ?? $user->id_type ?? null,
                'name'          => $types->name ?? null,
              ),
              'id_promotor' => array(
                'id'            => $promotors->id ?? $user->id_promotor ?? null,
                'name'          => $promotors->name ?? null,
              ),
              'id_customer' => array(
                'id'            => $customers->id ?? $user->id_customer ?? null,
                'name'          => $customers->name ?? null,
              ),
              'created_by' => array(
                'id'            => $users->id ?? $user->created_by ?? null,
                'name'          => $users->name ?? null,
              ),
            ]);
          });

          return $data;

        });

        if (is_null($data)) {
          return $this->sendError('Order not found.');
        }

        return $this->sendResponse($data->toArray(), 'All records retrieved successfully.');
      } catch (\Throwable $th) {
        return $this->sendError($th->getMessage());
      }
    }
}