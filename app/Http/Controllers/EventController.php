<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\Talent;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Google\Cloud\Storage\StorageClient;
use Illuminate\Support\Facades\Cache;

class EventController extends BaseController
{


  public function __construct()
  {
    // $this->middleware('auth:api', ['except' => ['store', 'index', 'detail', 'update', 'destroy']]);
  }
  /**
   * Get a JWT via given credentials.
   *
   * @param  Request  $request
   * @return Response
   */

  public function index(Request $request)
  {
    try {
      // $cacheKey = 'event_' . md5(serialize($request->all()));

      // Cek apakah data ada di cache Redis
      $data = Cache::remember('event', 5 * 60, function () use ($request) {
        $data = Event::query();
        $field = DB::getSchemaBuilder()->getColumnListing('event');

        if ($request->search) :
          $search = $request->search ?? '';
          $data = $data->whereHas('promotor', function ($query) use ($search) {
            $fieldProduct = DB::getSchemaBuilder()->getColumnListing('promotor');
            $query->where(DB::raw('LOWER(CAST(' . $fieldProduct[0] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
            for ($i = 1; $i < count($fieldProduct); $i++) :
              $query->orWhere(DB::raw('LOWER(CAST(' . $fieldProduct[$i] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
            endfor;
          })->orWhereHas('promotor', function ($query) use ($search) {
            $fieldProduct = DB::getSchemaBuilder()->getColumnListing('promotor');
            $query->where(DB::raw('LOWER(CAST(' . $fieldProduct[0] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
            for ($i = 1; $i < count($fieldProduct); $i++) :
              $query->orWhere(DB::raw('LOWER(CAST(' . $fieldProduct[$i] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
            endfor;
          })->orWhere(function ($query) use ($search, $field) {
            for ($i = 0; $i < count($field); $i++) :
              $query->orWhere(DB::raw('LOWER(CAST(' . $field[$i] . ' AS TEXT))'), 'LIKE', '%' . strtolower($search) . '%');
            endfor;
          });
        endif;

        if ($request->has('filter') && is_array($request->input('filter'))) {
          $filter = $request->input('filter');
          $data->where(function ($query) use ($filter) {
            foreach ($filter as $key => $value) {
              $query->where(DB::raw('LOWER(CAST(' . $key . ' AS TEXT))'), strtolower($value));
            }
          });
        }

        $data = $data->orderBy($request->orderBy ?? 'id', $request->orderSort ?? 'asc');
        $data = $data->paginate($request->limit ?? $data->count(), ['*'], 'page', $request->page ?? '1');


        if (is_null($data)) {
          return null;
        }


        $data->getCollection()->transform(function ($event) {
          $eventArray = $event->toArray();
          $users = DB::table('users')->whereId($event->created_by)->first();
          $promotors = $event->promotor()->first();

          $id_talents = isset($event->id_talent) ? explode(',', $event->id_talent) : [];
          $talents = [];
          if (!empty($id_talents)) {
              $talents = Talent::whereIn('id', $id_talents)->get();
          }

          $talent_data = $talents->map(function ($talent) {
              return [
                  'id' => $talent->id,
                  'name' => $talent->name,
              ];
          });

          $storageClient = new StorageClient([
            'projectId' => env('GOOGLE_CLOUD_PROJECT_ID'),
            'keyFilePath' => base_path(env('GOOGLE_CLOUD_KEY_FILE', 'edc-hepytech-9561c5f11ba9.json')),
          ]);

          // Dapatkan nama bucket dan objek
          $bucketName = env('GOOGLE_CLOUD_STORAGE_BUCKET');
          $objectName = $event->img_layout; // Sesuaikan dengan struktur penyimpanan Anda di GCS
          
          // Bangun URL dasar
          $img_layout_url = "https://storage.googleapis.com/{$bucketName}/{$objectName}";
          
          return array_merge($eventArray, [
            'id_talent' => $talent_data->toArray(),
            'id_promotor_created' => array(
              'id'            => $promotors->id ?? $event->id_promotor_created ?? null,
              'created_at'  => $promotors->created_at ?? null,
            ),
            'img_layout'        => $img_layout_url,
            'created_by' => array(
              'id'            => $users->id ?? $event->created_by ?? null,
              'name'          => $users->name ?? null,
            ),
          ]);
        });

        return $data;
      });

      if (is_null($data)) {
        return $this->sendError('Event not found.');
      }

      return $this->sendResponse($data->toArray(), 'All records retrieved successfully.');
    } catch (\Throwable $th) {
      return $this->sendError($th->getMessage());
    }
  }

  public function store(Request $request)
  {
    try {
      $input = $request->all();

      $validator = Validator::make($input, [
        "id_talent" => 'required',
        "title"       => 'required',
        "desc"       => 'required',
        "date"       => 'required',
        "location"       => 'required',
        "sk"       => 'required',
        "tag"       => 'required',
        "id_promotor_created"       => 'required',
        "img_layout"       => 'required|mimes:svg,png,jpg,jpeg',
      ]);

      if ($validator->fails()) {
        return $this->sendError($validator->errors()->first());
      }

      $filename = '';
        if ($request->hasFile('img_layout')) {
          if ($request->id != "0") {
            $find = Event::find($request->id);
            if ($find && Storage::disk('gcs')->exists($find->img_layout)) {
              Storage::disk('gcs')->delete($find->img_layout);
            }
          }
          
          $filename = $this->uploadFile(
            $request->file('img_layout'),
            Str::random(25) . '-event-layout-' . date('dmy')
          );
          } else {
            $filename = $request->img_layout_old ?? '';
          }

      // $id_talent = DB::table('talent')->where('deleted_at', null)->where('id', $request->id_talent)->first();

      // if (!$id_talent) {
      //   return $this->sendError('Invalid Talent.');
      // }
      
      $id_promotor_created = DB::table('promotor')->where('deleted_at', null)->where('id', $request->id_promotor_created)->first();

      if (!$id_promotor_created) {
        return $this->sendError('Invalid Promotor.');
      }

      // DB::beginTransaction();
      $form_auth = array(
        'id_talent' => $request->id_talent,
        'title' => $request->title,
        'desc' => $request->desc,
        'date' => $request->date,
        'location' => $request->location,
        'sk' => $request->sk,
        'tag' => $request->tag,
        'id_promotor_created' => $id_promotor_created->id,
        'img_layout'        => $filename,
        'slug'              => Str::slug($request->title, '-'),
        'flag'       => '1',
        'created_at'        => date('Y-m-d H:i:s')
      );

      Event::insertGetId($form_auth);
      // DB::Commit();
      return $this->sendResponse($form_auth, 'Event created successfully.');
    } catch (\Throwable $th) {
      // DB::rollback();
      return $this->sendError($th->getMessage());
    }
  }

  public function detail($slug = null)
  {
    try {
      $result = [];

      $data   = Event::where('slug', $slug)->first();

      if (is_null($data)) {
        return $this->sendError('Event not found.');
      }

      $dataArray = $data->toArray();
      $users = DB::table('users')->whereId($data->created_by)->first();
      $promotors = $data->promotor()->first();

      $id_talents = $data->id_talent;

      $id_talent_array = isset($id_talents) ? explode(',', $id_talents) : [];

      $talent_data = Talent::whereIn('id', $id_talent_array)->get();

      $transformedData = array_merge($dataArray, [
        'id_talent' => $talent_data->map(function ($talent) {
            return [
              'id' => $talent->id,
              'name' => $talent->name,
            ];
        })->toArray(),
        'id_promotor_created' => array(
          'id'            => $promotors->id ?? $promotors->id_promotor_created ?? null,
          'created_at'          => $promotors->created_at ?? null,
        ),
        'img_layout'        => isset($data->img_layout) ? URL::to('/') . 'public/uploads/event' . $data->img_layout : null,
        'created_by' => [
          'id'   => $users->id ?? $data->created_by ?? null,
          'name' => $users->name ?? null,
        ],
      ]);

      return $this->sendResponse($transformedData, 'Event by Detail retrieved successfully.');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  public function update(Request $request, $id = null)
  {
    try {
      $input = $request->all();

      $validator = Validator::make($input, [
        "id_talent" => 'required',
        "title"       => 'required',
        "desc"       => 'required',
        "date"       => 'required',
        "location"       => 'required',
        "sk"       => 'required',
        "tag"       => 'required',
        "id_promotor_created"       => 'required',
        "img_layout"       => 'required|mimes:svg,png,jpg,jpeg',
      ]);

      if ($validator->fails()) {
        return $this->sendError($validator->errors()->first());
      }

      $img_layout = null;

      if ($request->hasFile('img_layout')) {
        $file = $request->file('img_layout');
        if ($file->isValid()) {
          $maxSize = 5 * 1024 * 1024; // Batas ukuran file 5MB
          if ($file->getSize() > $maxSize) {
            return $this->sendError('File size exceeds the maximum allowed size.');
          }
          $img_layout  = time() . '.' . $file->getClientOriginalExtension();
          $file->move(base_path('public/uploads/event'), $img_layout);
        } else {
          return $this->sendError('Invalid Image file.');
        }
      }

      $talentid = DB::table('talent')->where('deleted_at', null)->find($request->id_talent);
      if (!$talentid) {
        return $this->sendError('Invalid Talent.');
      }

      $promotorid = DB::table('promotor')->where('deleted_at', null)->find($request->id_promotor_created);
      if (!$promotorid) {
        return $this->sendError('Invalid Talent.');
      }

      $data = Event::whereId($id)->first();
      $form = array(
        'id_talent' => $request->id_talent ?? $data->id_talent,
        'title' => $request->title ?? $data->title,
        'desc' => $request->desc ?? $data->desc,
        'date' => $request->date ?? $data->date,
        'location' => $request->location ?? $data->location,
        'sk' => $request->sk ?? $data->sk,
        'tag' => $request->tag ?? $data->tag,
        'id_promotor_created' => $request->id_promotor_created ?? $data->id_promotor_created,
        'slug'              => Str::slug($request->title, '-') ?? $data->title,
        'updated_at'        => date('Y-m-d H:i:s')
      );

      if ($img_layout !== null) {
        $form['img_layout'] = $img_layout;
      }

      DB::table('event')->where('id', $id)->update($form);

      return $this->sendResponse($form, 'Event updated successfully.');
    } catch (\Exception $th) {
      return $this->sendError($th->getMessage());
    }
  }

  public function destroy(Request $request, $id = null)
  {
    try {
      $event = Event::find($id);

      if (!$event) {
        throw new \Exception("Event with ID $id not found / has been deleted");
      }

      $form = array(
        'id_talent' => $event->id_talent,
        'title' => $event->title,
        'desc' => $event->desc,
        'date' => $event->date,
        'location' => $event->location,
        'sk' => $event->sk,
        'tag' => $event->tag,
        'id_promotor_created' => $event->id_promotor_created,
        'img_layout' => $event->img_layout,
      );

      $event->delete();

      return $this->sendResponse($form, 'Event delete successfully.');
    } catch (\Throwable $th) {
      return $this->sendError($th->getMessage());
    }
  }
}
