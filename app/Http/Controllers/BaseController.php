<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class BaseController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message = '')
    {
        $response = [
            'success' => true,
            'code'    => 200,
            'message' => $message,
            'result'  => $result,
        ];

        return response()->json($response, 200);
    }

    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'success' => false,
            'code'    => $code,
            'message' => $error,
            'result'  => !empty($errorMessages) ? $errorMessages : null
        ];

        return response()->json($response, $code);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $currentPage    = LengthAwarePaginator::resolveCurrentPage();
        $currentItems   = array_slice($items, $perPage * ($currentPage - 1), $perPage);
        return new LengthAwarePaginator($currentItems, count($items), $perPage, $currentPage);
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function BaseHeader($type = '')
    {
        $headers = [];
        if ($type === 'payment') {
            $headers = [
                'Accept'                        => 'application/json',
                "Content-Type"                  => "application/json",
                "Access-Control-Allow-Origin"   => "*",
                "Access-Control-Allow-Methods"  => "GET",
                'secret-key'                    => env('PAYMENT_SECRET_KEY', ''),
            ];
        }

        return $headers;
    }

    public function callImageGCP($image)
    {
        if ($image !== '' && Storage::disk('gcs')->exists($image)) {
            return Storage::disk('gcs')->url($image);
        } else {
            return null;
        }
    }

    public function uploadFile(UploadedFile $file, $folder = null, $filename = null)
    {
        $name = !is_null($filename) ? $filename : Str::random(25);
        $path = $folder ? $folder . '/' . $name . '.' . $file->getClientOriginalExtension() : $name . '.' . $file->getClientOriginalExtension();
        
        $contents = file_get_contents($file);
        Storage::disk('gcs')->write($path, $contents);

        return $path;
    }
}
