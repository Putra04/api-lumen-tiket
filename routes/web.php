<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
  // USER
  $router->post('/login', 'AuthController@login');
  $router->post('/register', 'AuthController@store');

  $router->get('/show', 'AuthController@index');
  
  // BANNER
  $router->get('/banner/show', 'BannerController@index');
  $router->post('/banner/add', 'BannerController@store');
  $router->get('/banner/show/{id}', 'BannerController@detail');
  $router->post('/banner/update/{id}', 'BannerController@update');
  $router->delete('/banner/delete/{id}', 'BannerController@destroy');

  // PROMOTOR
  $router->get('/promotor/show', 'PromotorController@index');
  $router->post('/promotor/add', 'PromotorController@store');
  $router->get('/promotor/show/{id}', 'PromotorController@detail');
  $router->post('/promotor/update/{id}', 'PromotorController@update');
  $router->delete('/promotor/delete/{id}', 'PromotorController@destroy');

  // EVENT
  $router->get('/event/show', 'EventController@index');
  $router->get('/event/detail/{slug}', 'EventController@detail');
  $router->post('/event/add', 'EventController@store');
});

$router->group(['prefix' => 'api', 'middleware' => 'auth'], function () use ($router) {
  // EVENT
  $router->post('/event/update/{id}', 'EventController@update');
  $router->delete('/event/delete/{id}', 'EventController@destroy');

  // ORDER
  $router->get('/order/show', 'OrderController@index');
  $router->post('/order/add', 'OrderController@store');
  $router->get('/order/show/{id}', 'OrderController@detail');
  $router->post('/order/update/{id}', 'OrderController@update');
  $router->delete('/order/delete/{id}', 'OrderController@destroy');
});
