<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('event', function (Blueprint $table) {
            $table->id();
            $table->integer('id_talent')->nullable();
            $table->string('title')->nullable();
            $table->text('desc')->nullable();
            $table->date('date')->nullable();
            $table->string('location')->nullable();
            $table->text('sk')->nullable();
            $table->string('tag')->nullable();
            $table->integer('id_promotor_created')->nullable();
            $table->text('img_layout')->nullable();
            $table->integer('flag')->default(1);
            $table->integer('created_by')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('event');
    }
};
