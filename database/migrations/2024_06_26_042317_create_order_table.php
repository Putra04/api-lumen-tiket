<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->integer('id_order_detail')->nullable();
            $table->string('disc')->nullable();
            $table->integer('id_fee')->nullable();
            $table->string('tax')->nullable();
            $table->string('grandtotal')->nullable();
            $table->enum('status', ['Pending', 'Onprocess', 'Paid', 'Cancel'])->default('Pending');
            $table->integer('flag')->default(1);
            $table->integer('created_by')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('order');
    }
};
