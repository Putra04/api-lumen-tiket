<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data =
        DB::table('users')->insert([
            'name' => 'Asep',
            'email' => 'asep@customer.com',
            'id_type' => 3,
            'id_promotor' => 1,
            'password' => Hash::make('password'),
            'id_customer' => 0,
            'flag' => 1,
            'created_by' => 1,
            'created_at' => date('Y-m-d H:i:s'),
        ]);

    }
}
